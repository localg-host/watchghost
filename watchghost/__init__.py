# WatchGhost, your invisible but loud monitoring pet
# Copyright © 2015 Kozea

import os

import aiohttp_jinja2
import jinja2
from aiohttp.web import Application

jinja2_loader = jinja2.FileSystemLoader(
    os.path.join(os.path.dirname(__file__), "templates")
)


class WatchghostApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.websockets = []
        self.config_dir = None

    async def startup(self):
        await super().startup()
        from . import config

        config.read(self.config_dir)


app = WatchghostApplication()
aiohttp_jinja2.setup(app, loader=jinja2_loader)
